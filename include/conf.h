#pragma once
#include "generalConf.h"

//*** LoRa Stuff ***
#define LORA_CS 5
#define LORA_IRQ 4
#define LORA_RST 16

#define I2C_SCL 0
#define I2C_SDA 32

#define FIRE0 14
#define FIRE1 27

#define CONF0 26
#define CONF1 25
#define CONF2 33

#define LED_SIG 13
#define LED_ARM 12

#define CHG1 15
#define CHG2 2
#define VBAT 36

#define VBAT_ADC_LOW 3200
#define VBAT_ADC_HIGH 3660

//#define USE_WDT
#define WDT_TIMEOUT 2

#define S1COLOR TFT_BLUE
#define S2COLOR TFT_ORANGE
#define S3COLOR TFT_RED

#define RSSI_COLOR TFT_ORANGE
#define BATTERY_COLOR TFT_CYAN


#define GENERAL_OFFSET 12