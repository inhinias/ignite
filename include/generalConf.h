#pragma once
#include "commands.h"

#define LORA_FREQ 868E6
#define DISABLE_AGC

#define ADDR_MASTER 0x69
#define ADDR_BROADCAST 0xaa
#define ADDR_SLAVE_BASE 0x0f
#define ADDR_SLAVE(slaveNum) (0x0f | (slaveNum << 4))

#define VBAT_SMOOTHING 50
#define VBAT_CHG_THRESH 2

#define FIRE_CMD_DELAY 50
#define PING_DELAY 50