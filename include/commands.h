#pragma once

//Commands
/*
The Commands are shortened down to a single byte in order to save data

Ping:
    This command is sent periodically by the master and repeated back by the slave.
    The slave appends to the command its battery level and RSSI.
    There are some variables for calculating packet loss statics for pings.
Fire:
    Continuosly sent when firing untill a ack or done is received.
Done:
    Firing is terminated
Ack:
    The Firing command has been received.
Batt:
    Sends the remote battery level.
*/
#define CMD_PING "P"
#define CMD_FIRE "F"
#define CMD_DONE "D"
#define CMD_ACK  "A"    
#define CMD_BATT "B"