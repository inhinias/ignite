#pragma once
#include "generalConf.h"


//*** LoRa Stuff ***
#define LORA_CS 10
#define LORA_IRQ 6
#define LORA_RST 14

#define I2C_SCL 19
#define I2C_SDA 18

#define FIRE0 2
#define FIRE1 2

#define LED_SIG 3
#define LED_ARM 12

#define CHG1 4
#define CHG2 5
#define VBAT 15

#define VBAT_ADC_LOW 830
#define VBAT_ADC_HIGH 915


//If the measured current drops below this threshold
//then the resistor is considered burned/inactive
//Value in Miliamps, float
#define CURRENT_THD 10.0