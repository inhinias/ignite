#pragma once

#ifdef IS_ATMEGA
#include "conf_slave.h"
#include <ArduinoSTL.h>
#else
#include "conf.h"
#endif
#include <Arduino.h>
#include <SPI.h>
#include <LoRa.h>
#include <vector>

#define MAX_PING_DIFF 4

typedef struct{
	String message;	// The actual content of the packet
	byte msgCount;      // Keep count of all messages for packet loss
	byte addrLoc;  // Address of the sender
	byte addrDest;   // Address of the receiver
} packet;

enum E_LoRaState{
	eLoRaOffline = 0,
	eLoRaSearching,
	eloRaOnline
};

enum E_FiringState{
	ePreparing = 0,
	eReadyToFire,
	eFiring,
	eDone,
	eFault
};

typedef struct{
	uint8_t rssi;		//Master----RSSI---->Slave
	uint8_t batLevel;	//Remote battery level
} remoteData;

typedef struct{
	bool dataChanged;
	E_LoRaState loraState;
	std::vector<uint8_t> rssi; //Master<----RSSI----Slave
	uint8_t batLevel;
	E_FiringState firingState;
	uint8_t remoteBatLevel; //Remote Battery Level
} displayData;

class LoRaWrap{
    public:
        LoRaWrap(uint8_t&);
        ~LoRaWrap();
        void sendPacket(packet);
        void ping();
		void ack();
		void batt();
        void setOwnAddr(uint8_t);
		void onReceive();

        displayData m_data;
		bool m_ackDone = false;
		bool m_ackFire = false;

    private:
        void pingReceived(uint8_t);  // Call this upon receiving a ping response
        //void onReceive(int packetSize);

        uint8_t m_ownAddr = 0x00;  // The address of this device
        uint8_t m_packetsSent = 0; // The number of packets sent
        uint8_t m_pingDifference = 0; //++ when a ping is sent -- when one is received
};

