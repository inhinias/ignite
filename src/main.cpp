#include <Arduino.h>
#include <SPI.h>

#include <Wire.h> //Somehow INA219 wont compile without this

#include <math.h>

#ifdef IS_ATMEGA
#include "conf_slave.h"
#else
#include "conf.h"
#endif
#include "loraWrapper.h"

#ifdef IS_ATMEGA
#include "Adafruit_INA219.h"
#else
#include <TFT_eSPI.h>
#include <esp_task_wdt.h>
#endif



//*** Function Prototypes ***
void reportError();
void reportError(String);
#ifdef IS_ATMEGA
Adafruit_INA219 ina219;
#else
void pushDisplayUpdate(displayData&);
void updateLoRaState(displayData&);
void updateMasterBatteryLevel(displayData&);
void updateFiringState(displayData&);
void updateRemoteData(displayData&);

//*** Global class instances ***
TFT_eSPI dsp = TFT_eSPI();

#endif

LoRaWrap* lora;


//***************************************************************
//HEY! THE SETUP FUNCTION IS HERE!!!!
//***************************************************************
void setup(){
	Serial.begin(115200);
	//Setup the display
	
	#ifndef IS_ATMEGA
	dsp.init();
    dsp.setRotation(3);
    dsp.fillScreen(TFT_BLACK);
    dsp.setTextColor(TFT_WHITE);
    dsp.setTextSize(2);
	#endif

	//Init I/O
	#ifndef IS_ATMEGA
	pinMode(CONF0, INPUT);
	pinMode(CONF1, INPUT);
	pinMode(CONF2, INPUT);
	#endif

	#ifdef IS_ATMEGA
	pinMode(FIRE0, OUTPUT);
	//pinMode(FIRE1, OUTPUT);
	digitalWrite(FIRE0, HIGH); //Instantly define the output to disable the P-MOSFET

	#else
	pinMode(FIRE0, INPUT);
	pinMode(FIRE1, INPUT);
	#endif

	pinMode(CHG1, INPUT);
	pinMode(CHG2, INPUT);
	pinMode(VBAT, INPUT);

	Serial.println("Initialized IO");

	//Init lora
	uint8_t loraStatus = 1;
	lora = new LoRaWrap(loraStatus);
	if(loraStatus){ //The connection to the module failed need to reboot!
		reportError();
		lora->m_data.loraState = eLoRaOffline;
		while(true);
	}

	#ifdef IS_ATMEGA
	else{
		digitalWrite(LED_SIG, HIGH);
	}

	if(!ina219.begin()) {
		//Current cannot be measured
		//Therefore the burning od the resistor cannot be measured
		reportError();
		Serial.println("Unable to communicate with INA219");
		lora->m_data.loraState = eLoRaOffline;
		while(true);
	}
	#endif


	//Configure based on the CONF inputs
	#ifndef IS_ATMEGA
	if(!digitalRead(CONF0) && !digitalRead(CONF1) && !digitalRead(CONF2)){
		lora->setOwnAddr(ADDR_MASTER);
	}
	else
	#endif
	{
		//lora->setOwnAddr(digitalRead(CONF0) + digitalRead(CONF1)*2 + digitalRead(CONF2)*4);
		lora->setOwnAddr(ADDR_SLAVE(1));
	}

	uint16_t adcLevel = analogRead(VBAT);
	if(adcLevel < VBAT_ADC_LOW){
		#ifdef IS_ATMEGA
		reportError();
		#else
		reportError("Akku Leer!");
		#endif
		while(true);
	}

	lora->m_data.loraState = eLoRaSearching;

	#ifdef USE_WDT
	esp_task_wdt_init(WDT_TIMEOUT, true); //enable WDT
  	esp_task_wdt_add(NULL); //add current thread to WDT
	esp_task_wdt_reset();
	#endif

	#ifndef IS_ATMEGA
	pushDisplayUpdate(lora->m_data);
	#endif
}

void loop(){
	lora->onReceive();

	//This state is when a physical connection to the lora module failed.
	//So nothing happens here
	//In theory this block will never be called cause a 
	//infinite while in setup() blocks any further execution
	if(lora->m_data.loraState == eLoRaOffline);
	else{
		//Not much happens here either, waiting to RECEIVE a ping.
		if(lora->m_data.loraState == eLoRaSearching){
			if(lora->m_data.firingState != ePreparing){
				lora->m_data.dataChanged = true;
				lora->m_data.firingState = ePreparing;
			}
		}

		if(lora->m_data.loraState == eloRaOnline){
			if(lora->m_data.firingState == ePreparing){
				lora->m_data.dataChanged = true;
				lora->m_data.firingState = eReadyToFire;
			}
		}

		if(lora->m_data.firingState == eReadyToFire){
			#ifndef IS_ATMEGA
			if(digitalRead(FIRE0) && digitalRead(FIRE1)){
				if(!lora->m_ackFire){
					static uint8_t fireCmdDelayCounter;
					if(fireCmdDelayCounter >= FIRE_CMD_DELAY){ //Wait FIRE_CMD_DELAY till it actually fires
						packet firePacket;
						firePacket.message = CMD_FIRE;   //Send fire command
						firePacket.addrLoc = ADDR_MASTER;
						firePacket.addrDest = ADDR_SLAVE(1);

						lora->sendPacket(firePacket);
						fireCmdDelayCounter = 0; //Send the command every second
					}
					else{
						fireCmdDelayCounter++;
					}
				}
				else{
					lora->m_ackFire = false;
					lora->m_data.dataChanged = true;
					lora->m_data.firingState = eFiring;
				}
			}
			#endif
		}


		
		if(lora->m_data.firingState == eFiring){
			#ifdef IS_ATMEGA
			//If a fire signal is received trigger the mosfet till there is no more current measured
			digitalWrite(FIRE0, LOW);

			static uint8_t ackCmdDelayCounter;
			if(ackCmdDelayCounter > 0){
				ackCmdDelayCounter--;
			}
			else{
				lora->ack();
				ackCmdDelayCounter = FIRE_CMD_DELAY*2;
			}


			float current = ina219.getCurrent_mA();
			if(current < CURRENT_THD){
				digitalWrite(FIRE0, HIGH);

				static uint8_t doneDelay;
				if(doneDelay >= FIRE_CMD_DELAY*2){
					packet donePacket;
					donePacket.message = CMD_DONE;   //Send done command
					donePacket.addrLoc = ADDR_SLAVE(1);
					donePacket.addrDest = ADDR_MASTER;
					lora->sendPacket(donePacket);
					doneDelay = 0;
				}
				else{
					doneDelay++;
				}
			}
			#else
			//There is nothing to do on the master in the firing state, except waiting for a Done signal
			#endif
		}

		if(lora->m_data.firingState == eDone){
			#ifndef IS_ATMEGA
			static uint8_t ackCmdDelayCounter;
			if(ackCmdDelayCounter > 0){
				ackCmdDelayCounter--;
			}
			else{
				lora->ack();
				lora->m_ackDone = false;
				ackCmdDelayCounter = FIRE_CMD_DELAY*2;
			}
			#endif

			static bool wasUpdated;
			if(!wasUpdated){
				lora->m_data.dataChanged = true;
				lora->m_data.firingState = eDone;
				wasUpdated = true;
			}
		}


		#ifndef IS_ATMEGA
		//Only the master pings
		//The slave returns the pin when received
		static uint8_t pingDelayCounter;
		if(pingDelayCounter < PING_DELAY){
			pingDelayCounter++;
		}
		else{
			lora->ping();
			pingDelayCounter = 0;
		}
		#endif

		//Map battery levels form analog to percent
		uint16_t adcLevel = analogRead(VBAT);
		if(adcLevel >= VBAT_ADC_HIGH){
			adcLevel = VBAT_ADC_HIGH;
		}
		else if(adcLevel <= VBAT_ADC_LOW){
			adcLevel = VBAT_ADC_LOW;
		}
		uint8_t newLevel = map(adcLevel, VBAT_ADC_LOW, VBAT_ADC_HIGH, 0, 100);

		//Only update when the level has changed significantly
		#ifdef IS_ATMEGA
		static uint8_t batCounter;
		if(abs(lora->m_data.remoteBatLevel - newLevel) > VBAT_CHG_THRESH || batCounter == 0){
			//Send a battery packet when the level has changed
			lora->m_data.remoteBatLevel = newLevel;
			lora->batt();
			batCounter = 100;
		}
		else{
			batCounter--;
		}
		#else
		if(abs(lora->m_data.batLevel - newLevel) > VBAT_CHG_THRESH){
			lora->m_data.batLevel = newLevel;
			updateMasterBatteryLevel(lora->m_data);
		}
		#endif

		#ifndef IS_ATMEGA
		//Update Eye movement in prep state
		if(lora->m_data.firingState == ePreparing){
			static uint8_t moveEyes;
			if(moveEyes < 200){ //Every 2s change the eye position
				moveEyes++;
			}
			else{
				updateFiringState(lora->m_data);
				moveEyes = 0;
			}
		}
		#endif

		#ifndef IS_ATMEGA
		static uint8_t lastRssi;
		static uint8_t lastBatLevel;
		if(abs(lastRssi-lora->m_data.rssi.at(0)) > 2 || abs(lastBatLevel - lora->m_data.batLevel) > VBAT_CHG_THRESH){
			lastRssi = lora->m_data.rssi.at(0);
			lastBatLevel = lora->m_data.batLevel;

			updateRemoteData(lora->m_data);
		}

		if(lora->m_data.dataChanged){
			updateFiringState(lora->m_data);
			updateLoRaState(lora->m_data);
			lora->m_data.dataChanged = false;
		}
		#endif
	}

	#ifdef USE_WDT
	esp_task_wdt_reset();
	#endif
	delay(10);
}

#ifndef IS_ATMEGA
void pushDisplayUpdate(displayData& data){
	dsp.fillScreen(TFT_BLACK);
	dsp.fillRect(0, 50, 320, 5, TFT_DARKGREY);
	dsp.setTextSize(1);
	dsp.setTextFont(4);
	dsp.drawRect(GENERAL_OFFSET, 55+GENERAL_OFFSET, 320-2*GENERAL_OFFSET, 240-55-2*GENERAL_OFFSET, S1COLOR);

	updateLoRaState(data);
	updateMasterBatteryLevel(data);
	updateFiringState(data);
	updateRemoteData(data);

	//Reset text settings
	dsp.setTextDatum(TL_DATUM);
	dsp.setTextFont(1);
	dsp.setTextSize(1);

	data.dataChanged = false;
}

void updateLoRaState(displayData& data){
	dsp.fillRect(0, 0, 200, 49, TFT_BLACK);
	dsp.setTextSize(1);
	dsp.setTextFont(4);

	switch (data.loraState){
		case eLoRaOffline:
			dsp.drawString("Offline", GENERAL_OFFSET, GENERAL_OFFSET);
			break;
		case eLoRaSearching:
			dsp.drawString("Suchen", GENERAL_OFFSET, GENERAL_OFFSET);
			break;
		case eloRaOnline:
			dsp.drawString("Online", GENERAL_OFFSET, GENERAL_OFFSET);
			break;
		default:
			break;
	}
}

void updateMasterBatteryLevel(displayData& data){
	dsp.fillRect(200, 0, 120, 49, TFT_BLACK);
	dsp.fillRect(320-30, GENERAL_OFFSET, round(data.batLevel/5), 10, BATTERY_COLOR);
	dsp.drawRect(320-30-2, GENERAL_OFFSET-2, 24, 14, TFT_DARKGREY);	
}

void updateFiringState(displayData& data){
	dsp.fillRect(GENERAL_OFFSET+1, 55+1+GENERAL_OFFSET, 320-2-2*GENERAL_OFFSET, 60, TFT_BLACK);
	dsp.setTextDatum(TC_DATUM);
	dsp.setTextSize(1);
	dsp.setTextFont(4);

	//Display three slaves
	//dsp.drawRect(GENERAL_OFFSET, 55+GENERAL_OFFSET, (320-(4*GENERAL_OFFSET))/3, 240-55-(GENERAL_OFFSET*2), TFT_RED);
	//dsp.drawRect((320/2)-(320-(4*GENERAL_OFFSET))/6, 55+GENERAL_OFFSET, (320-(4*GENERAL_OFFSET))/3, 240-55-(GENERAL_OFFSET*2), TFT_ORANGE);
	//dsp.drawRect(320-GENERAL_OFFSET-(320-(4*GENERAL_OFFSET))/3, 55+GENERAL_OFFSET, (320-(4*GENERAL_OFFSET))/3, 240-55-(GENERAL_OFFSET*2), TFT_BLUE);

	uint8_t rand = random(0, 3);

	switch (data.firingState){
		case ePreparing:
			if(rand == 0){
				dsp.drawString("( ._.) Verbinden", 320/2, 90);
			}
			else if(rand == 1){
				dsp.drawString("(._.) Verbinden", 320/2, 90);
			}
			else if(rand == 2){
				dsp.drawString("(._. ) Verbinden", 320/2, 90);
			}
			else{
				dsp.drawString("( ._.) Verbinden", 320/2, 90);
			}
			break;
		case eReadyToFire:
			dsp.drawString("Bereit", 320/2, 90);
			break;
		case eFiring:
			dsp.drawString("Brennt", 320/2, 90);
			break;
		case eDone:
			dsp.drawString("Fertig", 320/2, 90);
			break;
		case eFault:
			dsp.drawString("Unbekannter Fehler", 320/2, 90);
			break;
		default:
			break;
	}

	//Reset text settings
	dsp.setTextDatum(TL_DATUM);
	dsp.setTextFont(1);
	dsp.setTextSize(1);
}

void updateRemoteData(displayData& data){
	dsp.fillRect(GENERAL_OFFSET+1, 240-1-1-10-10-4*GENERAL_OFFSET, 320-2-2*GENERAL_OFFSET, 4*GENERAL_OFFSET, TFT_BLACK);
	//Remote battery level
	//Battery Icon
	dsp.fillRect(2.5*GENERAL_OFFSET, 240-1-10-10-4*GENERAL_OFFSET, 20, 10, TFT_DARKGREY);
	dsp.fillRect(2.5*GENERAL_OFFSET+20, 240+2-10-10-4*GENERAL_OFFSET, 3, 4, TFT_DARKGREY);

	//Battery Level
	dsp.drawRect(6*GENERAL_OFFSET-2, 240-1-2-10-10-4*GENERAL_OFFSET, 320-9*GENERAL_OFFSET+4, 14, TFT_DARKGREY);
	dsp.fillRect(6*GENERAL_OFFSET, 240-1-10-10-4*GENERAL_OFFSET, map(data.remoteBatLevel, 0, 100, 0, 320-9*GENERAL_OFFSET), 10, BATTERY_COLOR);

	//RSSI indicator
	//RSSI Icon
	dsp.fillRect(2.5*GENERAL_OFFSET, 240-1-4-3*GENERAL_OFFSET, 4, 4, TFT_DARKGREY);
	dsp.fillRect(2.5*GENERAL_OFFSET+6, 240-1-6-3*GENERAL_OFFSET, 4, 6, TFT_DARKGREY);
	dsp.fillRect(2.5*GENERAL_OFFSET+12, 240-1-8-3*GENERAL_OFFSET, 4, 8, TFT_DARKGREY);
	dsp.fillRect(2.5*GENERAL_OFFSET+18, 240-1-10-3*GENERAL_OFFSET, 4, 10, TFT_DARKGREY);

	//Rssi Level
	dsp.drawRect(6*GENERAL_OFFSET-2, 240-1-2-10-3*GENERAL_OFFSET, 320-9*GENERAL_OFFSET+4, 14, TFT_DARKGREY);
	dsp.fillRect(6*GENERAL_OFFSET, 240-1-10-3*GENERAL_OFFSET, map(data.rssi.at(0), 0, 100, 0, 320-9*GENERAL_OFFSET), 10, RSSI_COLOR);
	dsp.fillRect(6*GENERAL_OFFSET+((320-9*GENERAL_OFFSET)/100)*15, 240-1-2-10-3*GENERAL_OFFSET, 3, 14, TFT_DARKGREY);
	/*dsp.setTextSize(2);
	dsp.drawString(String(data.rssi.at(0)), 6*GENERAL_OFFSET-2, 240-1-2-10-3*GENERAL_OFFSET);
	dsp.setTextSize(1);*/
}
#endif

void reportError(){
	#ifdef IS_ATMEGA
	//Todo: Change to blink animation
	digitalWrite(LED_SIG, LOW);
	#else
	dsp.fillScreen(TFT_BLACK);
	dsp.setTextDatum(MC_DATUM); //Center text on its center point
	dsp.drawString("( °_°) Something went wrong, reboot plz!", 320/2, 240/2); //Draw the error message on the center of the screen
	dsp.setTextDatum(TL_DATUM);
	#endif
}

void reportError(String errorText){
	#ifndef IS_ATMEGA
	dsp.fillScreen(TFT_BLACK);
	dsp.setTextDatum(MC_DATUM); //Center text on its center point
	dsp.drawString(errorText, 320/2, 240/2); //Draw the error message on the center of the screen
	dsp.setTextDatum(TL_DATUM);
	#endif
}

