#include "loraWrapper.h"

LoRaWrap::LoRaWrap(uint8_t& status){
	LoRa.setPins(LORA_CS, LORA_RST, LORA_IRQ);

	bool loraOnline = false;
	uint8_t setupCount = 0; //Count how many times lora.begin was called. If too much abort.
	do{
		if(setupCount >= 100){
            status = 1;
			break;
		}
		loraOnline = LoRa.begin(LORA_FREQ);
		setupCount++;
	}
	while(!loraOnline);

	LoRa.setSpreadingFactor(9);
	LoRa.setTxPower(20);
	LoRa.enableCrc();
	#ifdef DISABLE_AGC
	LoRa.setGain(5);
	#endif

	Serial.println("Lora Online");

    //LoRa.onReceive((void (*)(int)) &LoRaWrap::onReceive);
  	//LoRa.receive();

    status = 0;

	m_data.dataChanged = true;
	m_data.loraState = eLoRaSearching;
	m_data.rssi.resize(8);
	m_data.rssi.at(0) = 0;
	m_data.batLevel = 0;
	m_data.firingState = ePreparing; 
	m_data.remoteBatLevel = 0;
}

LoRaWrap::~LoRaWrap(){

}

void LoRaWrap::sendPacket(packet outgoing){
	//Only send the packet when there is enough space for it (1 Byte(255) max message length)
	if(outgoing.message.length() < 256){
		outgoing.msgCount = m_packetsSent;
		LoRa.beginPacket();                   // start packet
		LoRa.write(outgoing.addrDest);        // add destination address
		LoRa.write(outgoing.addrLoc);         // add sender address
		//LoRa.write(outgoing.msgCount);        // add message ID
		LoRa.write(outgoing.message.length());// add payload length
		LoRa.print(outgoing.message);         // add payload
		LoRa.endPacket();	                  // finish packet and send it
		m_packetsSent++;
		LoRa.receive();						  // go back into receive mode
	}
}

void LoRaWrap::ping(){
	#ifdef IS_ATMEGA
	packet pingPack;
	pingPack.message = CMD_PING;
	pingPack.addrLoc = ADDR_SLAVE(1);
	pingPack.addrDest = ADDR_MASTER;
	#else

	if(m_pingDifference > MAX_PING_DIFF && (m_data.firingState != eFiring || m_data.firingState != eDone)){ // after 2 Sec no return ping, disconnected except when firing
		if(m_data.firingState != ePreparing || m_data.loraState != eLoRaSearching){			
			m_data.firingState = ePreparing;
			m_data.loraState = eLoRaSearching;
			m_data.dataChanged = true;
			m_data.remoteBatLevel = 0;
			m_data.rssi.at(0) = 0;
		}
		else{
			m_data.remoteBatLevel = 0;
			m_data.rssi.at(0) = 0;
		}
	}

	packet pingPack;
	pingPack.message = CMD_PING;
	pingPack.addrLoc = ADDR_MASTER;
	pingPack.addrDest = ADDR_BROADCAST;

	if(m_pingDifference <= 250){ //Prevent an overflow
		m_pingDifference++;
	}
	#endif

	sendPacket(pingPack);

	//Do the overflow manually to prevent an accidental reset
	//Note: This is dumb, too bad!
}

void LoRaWrap::batt(){
	packet batPack;
	batPack.message = CMD_BATT + String(m_data.remoteBatLevel);
	batPack.addrLoc = ADDR_SLAVE(1);
	batPack.addrDest = ADDR_MASTER;
	sendPacket(batPack);
}

void LoRaWrap::ack(){
	packet ackPack;
	ackPack.message = CMD_ACK;
	#ifdef IS_ATMEGA
	ackPack.addrLoc = ADDR_SLAVE(1);
	ackPack.addrDest = ADDR_MASTER;
	#else
	ackPack.addrLoc = ADDR_MASTER;
	ackPack.addrDest = ADDR_SLAVE(1);
	#endif
	sendPacket(ackPack);
}

void LoRaWrap::setOwnAddr(uint8_t addr){
	m_ownAddr = addr;
}

//void LoRaWrap::onReceive(int packetSize){
void LoRaWrap::onReceive(){
	int packetSize = LoRa.parsePacket();
	if (packetSize == 0) return;          // if there's no packet, return

	// read packet header bytes:
	int recipient = LoRa.read();          // recipient address
	byte sender = LoRa.read();            // sender address
	//byte incomingMsgId = LoRa.read();     // incoming msg ID
	byte incomingLength = LoRa.read();    // incoming msg length

	String incoming = "";                 // payload of packet

	while (LoRa.available()) {            // can't use readString() in callback, so
		incoming += (char)LoRa.read();      // add bytes one by one
	}

	#ifdef DEBUG
	Serial.println("Received from: 0x" + String(sender, HEX));
	Serial.println("Sent to: 0x" + String(recipient, HEX));
	//Serial.println("Message ID: " + String(incomingMsgId));
	Serial.println("Message length: " + String(incomingLength));
	Serial.println("Message: " + incoming);
	Serial.println("RSSI: " + String(LoRa.packetRssi()));
	Serial.println("Snr: " + String(LoRa.packetSnr()));
	Serial.println();
	#endif

	if (incomingLength != incoming.length()) {   // check length for error
		Serial.println("error: message length does not match received length");
		return;                             // skip rest of function
	}

	// if the recipient isn't this device or broadcast,
	if ((recipient != m_ownAddr && recipient != ADDR_BROADCAST) || (recipient == ADDR_BROADCAST && sender == m_ownAddr)) {
		Serial.println("This message is not for me.");
		return;                             // skip rest of the function
	}

	if(incoming == CMD_PING){
		m_data.loraState = eloRaOnline;

		#ifndef IS_ATMEGA
		if(m_pingDifference > MAX_PING_DIFF){
			m_pingDifference = 0;
		}
		else if(m_pingDifference != 0){ //Prevent an underflow
			m_pingDifference--;
		}

		#else
		ping(); //Return the ping received from master
		#endif 
	}
	else if(incoming == CMD_FIRE){
		#ifdef IS_ATMEGA
		m_data.firingState = eFiring;
		#endif
	}
	else if(incoming == CMD_ACK){
		#ifdef IS_ATMEGA
		m_data.firingState = eDone;
		#else
		m_ackFire = true;
		#endif
	}
	else if(incoming == CMD_DONE){
		m_data.firingState = eDone;
	}
	else if(incoming.startsWith(CMD_BATT)){
		String battLevel = incoming.substring((sizeof(CMD_BATT)/sizeof(CMD_BATT[0]))-1);
		m_data.remoteBatLevel = battLevel.toInt(); //Update changes are detected in main, mapping from 0-100 done on slave
	}
	else{
		Serial.println("Unknown command: " + incoming);
	}

	m_data.rssi.at(0) = map(140+LoRa.packetRssi(), 0, 140, 0, 100); //Cause the RSSI is negative, we need to do an addtion
}